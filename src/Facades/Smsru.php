<?php

namespace Stkbt\Smsru\Facades;

use Illuminate\Support\Facades\Facade;

class Smsru extends Facade {
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'smsru'; }
}