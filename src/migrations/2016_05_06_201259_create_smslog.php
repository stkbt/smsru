<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmslog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smslog', function (Blueprint $table) {
            
            $table->increments('id');
            
            $table->string('to', 12);
            $table->string('message');
            
            $table->string('result_id', 32);
            $table->unsignedInteger('result_code');
            $table->unsignedInteger('balance');
            
            
            $table->timestamp('created_at');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('smslog');
    }
}
